# post-48-demo

Code for https://scvalex.net/posts/48/

## Build & run

```
cargo run -- experiment1
cargo run -- experiment2
```

## License

All code is dual licensed under the [MIT
license](https://opensource.org/licenses/MIT) and [Apache License
2.0](https://opensource.org/licenses/Apache-2.0).
