use anyhow::{anyhow, bail};

type OrError<T> = Result<T, anyhow::Error>;

fn main() -> OrError<()> {
    match std::env::args().nth(1).as_deref() {
        None => bail!("Pass at least one argument"),
        Some("experiment1") => experiment1()?,
        Some("experiment2") => experiment2()?,
        Some("experiment3") => experiment3()?,
        Some(str) => bail!("Unknown experiment: {}", str),
    }
    Ok(())
}

/// Execute a GET 1.1.1.1 request over HTTPS.  This blows up with the
/// following error:
///
/// ```
/// Error: https://1.1.1.1/: Dns Failed: parsing '1.1.1.1': invalid dns name
///
/// Caused by:
///     invalid dns name
/// ```
fn experiment1() -> OrError<()> {
    let resp: String = ureq::get("https://1.1.1.1").call()?.into_string()?;
    println!("Response:\n{}", resp);
    Ok(())
}

/// Try to make a `webpki::DnsNameRef` out of `1.1.1.1`.  This fails
/// with the expected error.
///
/// ```
/// Error: Failed to make a DNSNameRef: InvalidDnsNameError
/// ```
fn experiment2() -> OrError<()> {
    use webpki::DnsNameRef;
    let _: DnsNameRef = DnsNameRef::try_from_ascii_str("1.1.1.1")
        .map_err(|err| anyhow!("Failed to make a DNSNameRef: {:?}", err))?;
    Ok(())
}

/// Per https://github.com/algesten/ureq/issues/393#issuecomment-859676345
/// and https://scvalex.net/posts/48/, implement a workaround.
fn experiment3() -> OrError<()> {
    use certificate_verifier_no_hostname::CertificateVerifierNoHostname;
    use fixed_resolver::FixedResolver;
    use rustls::ClientConfig;
    use std::sync::Arc;
    use url::Url;

    // `original_url` is our original URL, `ip_addr` is the extracted
    // IP address, and `url` is the updated URL with the dummy
    // hostname.
    let original_url: Url = "https://1.1.1.1".parse()?;
    let ip_addr = original_url.host().unwrap().to_string();
    #[allow(clippy::redundant_clone)]
    let mut url = original_url.clone();
    url.set_host(Some("dummy-hostname-for-workaround"))?;

    // Create a TLS config with the default certificate roots and a
    // certificate verifier that ignores hostnames.
    //
    // TODO Configure just your own CA here to get any security from
    // the certificate verification.  Otherwise, any valid certificate
    // will be accepted.
    let tls_config = ClientConfig::builder().with_safe_defaults();
    let tls_config =
        tls_config.with_custom_certificate_verifier(Arc::new(CertificateVerifierNoHostname {
            trustroots: &webpki_roots::TLS_SERVER_ROOTS,
        }));
    let tls_config = tls_config.with_no_client_auth();

    // Execute the `ureq` call with the special TLS config and a
    // resolver that always returns `ip_addr`.
    //
    // The extra tricks here are that we have to specify a port so
    // that we can parse a `SocketAddr` (although it's not used for
    // anything), and we have to set the `Host` header in case the
    // remote host is dispatching on that.
    let resp: String = ureq::builder()
        .tls_config(Arc::new(tls_config))
        .resolver(FixedResolver {
            ip_addr: format!("{}:443", ip_addr).parse()?,
        })
        .build()
        .request_url("GET", &url)
        .set("Host", &ip_addr)
        .call()?
        .into_string()?;
    println!("Response: {} chars", resp.len());
    Ok(())
}

mod fixed_resolver {
    //! A DNS resolver that always returns the same value regardless of
    //! the host it was queried for.

    use std::net::SocketAddr;

    pub struct FixedResolver {
        pub ip_addr: SocketAddr,
    }

    impl ureq::Resolver for FixedResolver {
        fn resolve(&self, _netloc: &str) -> std::io::Result<Vec<SocketAddr>> {
            Ok(vec![self.ip_addr])
        }
    }
}

mod certificate_verifier_no_hostname {
    use rustls::{
        client::{ServerCertVerified, ServerCertVerifier},
        Certificate, Error as TlsError, ServerName,
    };
    use std::time::SystemTime;
    use webpki::{EndEntityCert, SignatureAlgorithm, TlsServerTrustAnchors};

    pub struct CertificateVerifierNoHostname<'a> {
        pub trustroots: &'a TlsServerTrustAnchors<'a>,
    }

    static SUPPORTED_SIG_ALGS: &[&SignatureAlgorithm] = &[
        &webpki::ECDSA_P256_SHA256,
        &webpki::ECDSA_P256_SHA384,
        &webpki::ECDSA_P384_SHA256,
        &webpki::ECDSA_P384_SHA384,
        &webpki::ED25519,
        &webpki::RSA_PSS_2048_8192_SHA256_LEGACY_KEY,
        &webpki::RSA_PSS_2048_8192_SHA384_LEGACY_KEY,
        &webpki::RSA_PSS_2048_8192_SHA512_LEGACY_KEY,
        &webpki::RSA_PKCS1_2048_8192_SHA256,
        &webpki::RSA_PKCS1_2048_8192_SHA384,
        &webpki::RSA_PKCS1_2048_8192_SHA512,
        &webpki::RSA_PKCS1_3072_8192_SHA384,
    ];

    impl ServerCertVerifier for CertificateVerifierNoHostname<'_> {
        /// Will verify the certificate is valid in the following ways:
        /// - Signed by a valid root
        /// - Not Expired
        ///
        /// Based on a https://github.com/ctz/rustls/issues/578#issuecomment-816712636
        fn verify_server_cert(
            &self,
            end_entity: &Certificate,
            intermediates: &[Certificate],
            _server_name: &ServerName,
            _scts: &mut dyn Iterator<Item = &[u8]>,
            ocsp_response: &[u8],
            _now: SystemTime,
        ) -> Result<ServerCertVerified, TlsError> {
            let end_entity_cert = webpki::EndEntityCert::try_from(end_entity.0.as_ref())
                .map_err(|err| TlsError::General(err.to_string()))?;

            let chain: Vec<&[u8]> = intermediates.iter().map(|cert| cert.0.as_ref()).collect();

            // Validate the certificate is valid, signed by a trusted root, and not
            // expired.
            let now = SystemTime::now();
            let webpki_now =
                webpki::Time::try_from(now).map_err(|_| TlsError::FailedToGetCurrentTime)?;

            let _cert: EndEntityCert = end_entity_cert
                .verify_is_valid_tls_server_cert(
                    SUPPORTED_SIG_ALGS,
                    self.trustroots,
                    &chain,
                    webpki_now,
                )
                .map_err(|err| TlsError::General(err.to_string()))
                .map(|_| end_entity_cert)?;

            if !ocsp_response.is_empty() {
                //trace!("Unvalidated OCSP response: {:?}", ocsp_response.to_vec());
            }
            Ok(ServerCertVerified::assertion())
        }
    }
}
